<?php
// Settings / Setup
define('EDULEARN_DEV_MODE', true);

// Includes Files
include(get_theme_file_path('/front/enqueue.php'));
include(get_theme_file_path('/front/setup.php'));


// Hooks
add_action('wp_enqueue_scripts', 'edulearn_enqueue');
add_action('after_setup_theme', 'edulearn_setup_theme');


function edulearn_menu_config(){
   register_nav_menus(
        array(
             'edulearn_main_menu' => 'Edu Learn Main Menu',
             'edulearn_sidebar_menu' => 'Edu Learn Sidebar Menu',

        )
     );
}
add_action('after_setup_theme', 'edulearn_menu_config');

 ?>
