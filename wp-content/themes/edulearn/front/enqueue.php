<?php
   function edulearn_enqueue(){
     $uri = get_theme_file_uri();
     $ver = EDULEARN_DEV_MODE ? time() : false;
     //CSS
     wp_register_style('bootstrap_css', $uri . '/assets/css/bootstrap.min.css', [], $ver);
     wp_register_style('fontawesome_css', $uri . '/assets/css/font-awesome.min.css', [], $ver);
     wp_register_style('animate_css', $uri . '/assets/css/animate.css', [], $ver);
     wp_register_style('owl_css', $uri . '/assets/css/owl.carousel.css', [], $ver);
     wp_register_style('slick_css', $uri . '/assets/css/slick.css', [], $ver);
     wp_register_style('magnific_css', $uri . '/assets/css/magnific-popup.css', [], $ver);
     wp_register_style('off_css', $uri . '/assets/css/off-canvas.css', [], $ver);
     wp_register_style('flaticon_css', $uri . '/assets/fonts/flaticon.css', [], $ver);
     wp_register_style('font_css', $uri . '/assets/fonts/fonts2/flaticon.css', [], $ver);
     wp_register_style('rsmenu_css', $uri . '/assets/css/rsmenu-main.css', [], $ver);
     wp_register_style('rs_css', $uri . '/assets/css/rsmenu-transitions.css', [], $ver);
     wp_register_style('style_css', $uri . '/assets/css/style.css', [], $ver);
     wp_register_style('responsive_css', $uri . '/assets/css/responsive.css', [], $ver);
     wp_enqueue_style('bootstrap_css');
     wp_enqueue_style('fontawesome_css');
     wp_enqueue_style('animate_css');
     wp_enqueue_style('owl_css');
     wp_enqueue_style('slick_css');
     wp_enqueue_style('magnific_css');
     wp_enqueue_style('off_css');
     wp_enqueue_style('flaticon_css');
     wp_enqueue_style('font_css');
     wp_enqueue_style('rsmenu_css');
     wp_enqueue_style('rs_css');
     wp_enqueue_style('style_css');
     wp_enqueue_style('responsive_css');

     // JS
     wp_register_script('modernizr_js', $uri. '/assets/js/modernizr-2.8.3.min.js', [], $ver, true);
     wp_register_script('bootstrap_js', $uri. '/assets/js/bootstrap.min.js', [], $ver, true);
     wp_register_script('owl_js', $uri. '/assets/js/owl.carousel.min.js', [], $ver, true);
     wp_register_script('slick_js', $uri. '/assets/js/slick.min.js', [], $ver, true);
     wp_register_script('pkgd_js', $uri. '/assets/js/isotope.pkgd.min.js', [], $ver, true);
     wp_register_script('image_js', $uri. '/assets/js/imagesloaded.pkgd.min.js', [], $ver, true);
     wp_register_script('wow_js', $uri. '/assets/js/wow.min.js', [], $ver, true);
     wp_register_script('way_js', $uri. '/assets/js/waypoints.min.js', [], $ver, true);
     wp_register_script('counter_js', $uri. '/assets/js/jquery.counterup.min.js', [], $ver, true);
     wp_register_script('pop_js', $uri. '/assets/js/jquery.magnific-popup.min.js', [], $ver, true);
     wp_register_script('rs_js', $uri. '/assets/js/rsmenu-main.js', [], $ver, true);
     wp_register_script('plugin_js', $uri. '/assets/js/plugins.js', [], $ver, true);
     wp_register_script('main_js', $uri. '/assets/js/main.js', [], $ver, true);
     wp_enqueue_script('jquery');
     wp_enqueue_script('modernizr_js');
     wp_enqueue_script('bootstrap_js');
     wp_enqueue_script('owl_js');
     wp_enqueue_script('slick_js');
     wp_enqueue_script('pkgd_js');
     wp_enqueue_script('image_js');
     wp_enqueue_script('wow_js');
     wp_enqueue_script('way_js');
     wp_enqueue_script('counter_js');
     wp_enqueue_script('pop_js');
     wp_enqueue_script('rs_js');
     wp_enqueue_script('plugin_js');
     wp_enqueue_script('main_js');
   }

 ?>
