<!-- Footer Start -->
<footer id="rs-footer" class="bg3 rs-footer"  style="background-image: url(<?php echo get_template_directory_uri() . '/assets/images/bg/counter-bg.jpg' ?>);">
<div class="container">
<!-- Footer Address -->
<div>
  <div class="row footer-contact-desc">
    <div class="col-md-4">
      <div class="contact-inner">
        <i class="fa fa-map-marker"></i>
        <h4 class="contact-title">Address</h4>
        <p class="contact-desc">
          503  Old Buffalo Street<br>
          Northwest #205, New York-3087
        </p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="contact-inner">
        <i class="fa fa-phone"></i>
        <h4 class="contact-title">Phone Number</h4>
        <p class="contact-desc">
          +3453-909-6565<br>
          +2390-875-2235
        </p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="contact-inner">
        <i class="fa fa-map-marker"></i>
        <h4 class="contact-title">Email Address</h4>
        <p class="contact-desc">
          infoname@gmail.com<br>
          www.yourname.com
        </p>
      </div>
    </div>
  </div>
</div>
</div>

<!-- Footer Top -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <div class="about-widget">
                        <img src="images/logo-footer.png" alt="Footer Logo">
                        <p>We create Premium Html Themes for more than three years. Our team goal is to reunite the elegance of unique.</p>
                        <p class="margin-remove">We create Unique and Easy To Use Flexible Html Themes.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12">
                    <h5 class="footer-title">RECENT POSTS</h5>
                    <div class="recent-post-widget">
                        <div class="post-item">
                            <div class="post-date">
                                <span>28</span>
                                <span>June</span>
                            </div>
                            <div class="post-desc">
                                <h5 class="post-title"><a href="index.html#">While the lovely valley team work</a></h5>
                                <span class="post-category">Keyword Analysis</span>
                            </div>
                        </div>
                        <div class="post-item">
                            <div class="post-date">
                                <span>28</span>
                                <span>June</span>
                            </div>
                            <div class="post-desc">
                                <h5 class="post-title"><a href="index.html#">I must explain to you how all this idea</a></h5>
                                <span class="post-category">Spoken English</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12">
                    <h5 class="footer-title">OUR SITEMAP</h5>
                    <ul class="sitemap-widget">
                        <li class="active"><a href="index.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Home</a></li>
                        <li ><a href="about.html"><i class="fa fa-angle-right" aria-hidden="true"></i>About</a></li>
                        <li><a href="courses.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Courses</a></li>
                        <li><a href="courses-details.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Courses Details</a></li>
                        <li><a href="events.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Events</a></li>
                        <li><a href="events-details.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Events Details</a></li>
                        <li><a href="blog.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Blog</a></li>
                        <li><a href="blog-details.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Blog Details</a></li>
                        <li><a href="teachers.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Teachers</a></li>
                        <li><a href="teachers-single.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Teachers Details</a></li>
                        <li><a href="contact.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Contact</a></li>
                        <li><a href="error-404.html"><i class="fa fa-angle-right" aria-hidden="true"></i>Error 404</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-12">
                    <h5 class="footer-title">NEWSLETTER</h5>
                    <p>Sign Up to Our Newsletter to Get Latest Updates &amp; Services</p>
                    <form class="news-form">
                        <input type="text" class="form-input" placeholder="Enter Your Email">
                        <button type="submit" class="form-button"><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            <div class="footer-share">
                <ul>
                    <li><a href="index.html#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="index.html#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="index.html#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="index.html#"><i class="fa fa-pinterest-p"></i></a></li>
                    <li><a href="index.html#"><i class="fa fa-vimeo"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Footer Bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="copyright">
                <p>© 2018 <a href="index.html#">RS Theme</a>. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer End -->

<!-- start scrollUp  -->
<div id="scrollUp">
    <i class="fa fa-angle-up"></i>
</div>

<!-- Canvas Menu start -->
<nav class="right_menu_togle">
  <div class="close-btn"><span id="nav-close" class="text-center">x</span></div>
    <div class="canvas-logo">
      <?php
         if(has_custom_logo() || is_customize_preview()){
           the_custom_logo();
         } else {
      ?>

        <a href="index.html"><img src="<?php echo get_template_directory_uri() . '/assets/images/logo.png' ?>" alt="logo"></a>

    <?php } ?>
    </div>
    <ul class="sidebarnav_menu list-unstyled main-menu">
      <?php
           if(has_nav_menu('edulearn_main_menu')){
               wp_nav_menu([
                 'theme_location' => 'edulearn_sidebar_menu',
                 'container' => '<ul>',
                 'menu_class' => 'nav-menu',
                 'fallback_cb' => false,
                 'depth' => 2,
               ]);
           }
       ?>
    </ul>
    <div class="search-wrap">
        <label class="screen-reader-text">Search for:</label>
        <input type="search" placeholder="Search..." name="s" class="search-input" value="">
        <button type="submit" value="Search"><i class="fa fa-search"></i></button>
    </div>
</nav>
<!-- Canvas Menu end -->

<!-- Search Modal Start -->
<div aria-hidden="true" class="modal fade search-modal" role="dialog" tabindex="-1">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true" class="fa fa-close"></span>
  </button>
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="search-block clearfix">
                <form>
                    <div class="form-group">
                        <input class="form-control" placeholder="eg: Computer Technology" type="text">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Search Modal End -->

<?php wp_footer(); ?>
</body>
</html>
