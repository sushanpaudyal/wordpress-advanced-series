<?php

add_action('init', 'edulearn_slider_post_type', 0 );
add_action('init', 'edulearn_team_post_type', 0 );
add_action('init', 'edulearn_testimonial_post_type', 0 );




function edulearn_slider_post_type() {
    // Labels for the Post Type
    $labels = array(
        'name'                => _x( 'Sliders', 'Post Type General Name', 'corp' ),
        'singular_name'       => _x( 'Slider', 'Post Type Singular Name', 'corp' ),
        'menu_name'           => __( 'Sliders', 'corp' ),
        'parent_item_colon'   => __( 'Parent Slider', 'corp' ),
        'all_items'           => __( 'All Sliders', 'corp' ),
        'view_item'           => __( 'View Slider', 'corp' ),
        'add_new_item'        => __( 'Add New Slider', 'corp' ),
        'add_new'             => __( 'Add New Slider', 'corp' ),
        'edit_item'           => __( 'Edit Slider', 'corp' ),
        'update_item'         => __( 'Update Slider', 'corp' ),
        'search_items'        => __( 'Search Slider', 'corp' ),
        'not_found'           => __( 'No sliders found', 'corp' ),
        'not_found_in_trash'  => __( 'Not found in trash', 'corp' ),
    );
    // Another Customizations
    $args = array(
        'label'   => __('Sliders','corp' ),
        'description' => __('Sliders for Mount', 'corp'),
        'labels'  => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menus' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 15,
        'menu_icon' => 'dashicons-format-gallery',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'capability_type' => 'page',
    );
    // register the post Type
    register_post_type( 'sliders', $args);
}





function edulearn_team_post_type() {
    // Labels for the Post Type
    $labels = array(
        'name'                => _x( 'Teams', 'Post Type General Name', 'corp' ),
        'singular_name'       => _x( 'Team', 'Post Type Singular Name', 'corp' ),
        'menu_name'           => __( 'Teams', 'corp' ),
        'parent_item_colon'   => __( 'Parent Team', 'corp' ),
        'all_items'           => __( 'All Teams', 'corp' ),
        'view_item'           => __( 'View Team', 'corp' ),
        'add_new_item'        => __( 'Add New Team', 'corp' ),
        'add_new'             => __( 'Add New Team', 'corp' ),
        'edit_item'           => __( 'Edit Team', 'corp' ),
        'update_item'         => __( 'Update Team', 'corp' ),
        'search_items'        => __( 'Search Team', 'corp' ),
        'not_found'           => __( 'No teams found', 'corp' ),
        'not_found_in_trash'  => __( 'Not found in trash', 'corp' ),
    );
    // Another Customizations
    $args = array(
        'label'   => __('Teams','corp' ),
        'description' => __('Teams for Mount', 'corp'),
        'labels'  => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menus' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 15,
        'menu_icon' => 'dashicons-groups',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'capability_type' => 'page',
    );
    // register the post Type
    register_post_type( 'teams', $args);
}





function edulearn_testimonial_post_type() {
    // Labels for the Post Type
    $labels = array(
        'name'                => _x( 'Testimonials', 'Post Type General Name', 'corp' ),
        'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'corp' ),
        'menu_name'           => __( 'Testimonials', 'corp' ),
        'parent_item_colon'   => __( 'Parent Testimonial', 'corp' ),
        'all_items'           => __( 'All Testimonials', 'corp' ),
        'view_item'           => __( 'View Testimonial', 'corp' ),
        'add_new_item'        => __( 'Add New Testimonial', 'corp' ),
        'add_new'             => __( 'Add New Testimonial', 'corp' ),
        'edit_item'           => __( 'Edit Testimonial', 'corp' ),
        'update_item'         => __( 'Update Testimonial', 'corp' ),
        'search_items'        => __( 'Search Testimonial', 'corp' ),
        'not_found'           => __( 'No testimonials found', 'corp' ),
        'not_found_in_trash'  => __( 'Not found in trash', 'corp' ),
    );
    // Another Customizations
    $args = array(
        'label'   => __('Testimonials','corp' ),
        'description' => __('Testimonials for Mount', 'corp'),
        'labels'  => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menus' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 15,
        'menu_icon' => 'dashicons-smiley',
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'capability_type' => 'page',
    );
    // register the post Type
    register_post_type( 'testimonials', $args);
}
