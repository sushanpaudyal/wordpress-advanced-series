<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'edulearn' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

 define('AUTH_KEY',         'P+>,TLLhAXNHnAB|~ni^l+D6<QY*<K-+;@-99mHoF*vVu*h#dc+M[to4-<OzJ{s@');
 define('SECURE_AUTH_KEY',  'Zw3PKl|4TCj,dKKs#z?l`IkkuTVe7Bf0QA0io3`jo=G9TM8$//a~%v+4c#.IUU=r');
 define('LOGGED_IN_KEY',    'aOIRda|N&A-{i|{>TOJ|NE2Y!vC-W(m!FR0D-!Pz&#]5Iy@f![t288DcJmq%|g7e');
 define('NONCE_KEY',        'T>Q.sK1Gpa(@0n&Eg9E&l^M*.Dl!KVO,O:e5iS*ud1F9$/Y3*ss<I291n1E@HsM&');
 define('AUTH_SALT',        '=]>YH[r-Kd)EIFGf>aN(zq3c+qX_cB7h&#{D--7g;iZ2adgPT}ln:Uke-LY?B_RJ');
 define('SECURE_AUTH_SALT', 'o)w+|1j={c,mS{H&;1K)_z]^E%+Jx#u-yF|cpLh=2 8gO++IfBN%t/[P$zKoB|&k');
 define('LOGGED_IN_SALT',   'I.e.)s6BY@Um4V~}>($nV>Cru7vDv1<9g*G[kprZj`!4JnA_<bt5b_hiw=!B1|-;');
 define('NONCE_SALT',       '[3xQs%(yF%|ogS![}!zCY|%X~&HL}|tvO5|{3^k&Va(!!bt_5cEt3OGt+v$g@=%}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
